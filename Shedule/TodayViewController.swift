//
//  TodayViewController.swift
//  Shedule
//
//  Created by Даниил Чемеркин on 06/09/2018.
//  Copyright © 2018 Danya. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreData

enum StepState: Int {
    case bachelor = 0
    case master
    case postgraduate
    
    var describeSelf: String {
        switch self {
        case .bachelor:
            return "bachelor"
        case .master:
            return "master"
        case .postgraduate:
            return "postgraduate"
        }
    }
}

class TodayViewController: UIViewController, NCWidgetProviding {
    
    lazy var corDataManager: CoreDataManager = {
        return CoreDataManager()
    }()
    
    var subjects = [LessonModel]() {
        didSet {
            guard collectionView != nil else { return }
            collectionView.reloadData()
        }
    }
    
    var collectionView: UICollectionView! {
        didSet {
            let nib = UINib(nibName: "SubjectCell", bundle: Bundle.main)
            collectionView.register(nib, forCellWithReuseIdentifier: "subjectCell")
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.backgroundColor = UIColor.red.withAlphaComponent(0)
            view.addSubview(collectionView)
        }
    }
    
    fileprivate var course: Int?
    fileprivate var group: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.course = UserDefaults.init(suiteName: "group.daminik00.sfedu")?.integer(forKey: "timetable.kourse")
        self.group = UserDefaults.init(suiteName: "group.daminik00.sfedu")?.integer(forKey: "timetable.group")
        setSubjects()
        setCollectionView()
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    func setSubjects() {
        subjects = [LessonModel]()
        let dayNumber = Date().dayNumberOfWeek() == 1 ? 2 : Date().dayNumberOfWeek()
        let changesIndex = dayNumber! - 2
        let currentWeek = UserDefaults.init(suiteName: "group.daminik00.sfedu")?.integer(forKey: "currentWeek")
        for i in 0..<corDataManager.countEntity(with: changesIndex) {
            guard let lessonData = corDataManager.fetchForModel(with: changesIndex, indexPath: i) else { continue }
            let localModel = LessonModel(
                timeSince: lessonData.value(forKey: "timeSince") as! String,
                timeBefore: lessonData.value(forKey: "timeBefore") as! String,
                room: lessonData.value(forKey: "room") as! String,
                teacherName: lessonData.value(forKey: "teacherName") as! String,
                subjectName: lessonData.value(forKey: "subjectName") as! String,
                isUp: lessonData.value(forKey: "isUpper") as! Int
            )
            if localModel.checkUpper(currentWeek: currentWeek!) {
                subjects.append(localModel)
            }
        }
        subjects = subjects.sorted(by: { (modelA, modelB) -> Bool in
            return modelA.timeSince.split(separator: ":")[0] < modelB.timeSince.split(separator: ":")[0]
        })
        preferredContentSize = CGSize(width: 0, height: subjects.count*50)
    }
    
    func setCollectionView() {
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: CGRect(
            x: 0,
            y: 0,
            width: view.bounds.width,
            height: view.bounds.height), collectionViewLayout: layout)
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded {
            preferredContentSize = CGSize(width: 0, height: subjects.count*50)
        } else {
            preferredContentSize = maxSize
        }
    }
    
    func downloadBefore() {
        let dataService = DataService()
        dataService.requestCurrentWeek { curWeek in
            guard curWeek != -1 else {
                print("Error. Current week = -1")
                return
            }
            UserDefaults.init(suiteName: "group.daminik00.sfedu")?.set(curWeek, forKey: "currentWeek")
            dataService.requestOfGradeList(with: self.course!, group: self.group!) {
                self.setSubjects()
            }
        }
    }
}

extension TodayViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subjects.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subjectCell", for: indexPath) as! SubjectCell
        cell.set(model: subjects[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width-0, height: 50)
    }
}
