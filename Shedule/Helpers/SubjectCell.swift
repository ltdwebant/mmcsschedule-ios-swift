//
//  CollectionViewCell.swift
//  Shedule
//
//  Created by Даниил Чемеркин on 07/09/2018.
//  Copyright © 2018 Danya. All rights reserved.
//

import UIKit

class SubjectCell: UICollectionViewCell {

    @IBOutlet weak var timeBeforeLabel: UILabel!
    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var time: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        timeLine = UIView()
    }
    
    
    var timeLine: UIView! {
        didSet {
            timeLine.frame = CGRect(x: 0, y: -3, width: bounds.width, height: 3)
            timeLine.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            addSubview(timeLine)
        }
    }
    var nowTime = 0 { // текущее время в секундах
        didSet {
            let lineY = getLineY(timeSec: nowTime)
            if lineY+3 >= 0 && lineY+3 < bounds.height {
                timeLine.frame.origin.y = lineY
                let time = countTimeBefore(nowTime: nowTime)
                timeBeforeLabel.text = "\(time.0):\(time.1):\(time.2)"
            } else {
                timeBeforeLabel.text = ""
                timeLine.frame.origin.y = -3
            }
        }
    }
    
    func countTimeBefore(nowTime seconds: Int) -> (Int, Int, Int) {
        let timeBeforeInSeconds = Int(timeBefore - CGFloat(nowTime))
        let hour = timeBeforeInSeconds / 3600
        let minute = timeBeforeInSeconds % 3600 / 60
        let sec = (timeBeforeInSeconds % 3600) % 60
        return (hour, minute, sec)
    }
    
    func getLineY(timeSec: Int) -> CGFloat {
//        print(CGFloat(timeSec) - timeSince, secAll, (CGFloat(timeSec) - timeSince)*100/secAll/100*bounds.height)
        return (CGFloat(timeSec) - timeSince)*100/secAll/100*CGFloat(bounds.height)-3
    }
    
    var timeBefore: CGFloat! // время от начала дня до конца занятия в секундах
    var timeSince: CGFloat! // время от начала дня до начала занятия в секундах
    var secAll: CGFloat! // сколько длиться занятие в секундах
    var timer: Timer!
    
    func set(model: LessonModel) {
        subject.text = "\(model.subjectName)"
        time.text = "\(model.timeSince)-\(model.timeBefore)"
        place.text = model.room
        let timeSince = Time(time: model.timeSince)
        let timeBefore = Time(time: model.timeBefore)
        let timeNow = Time.nowTime
        let since: Bool = ((timeNow.hour == timeSince.hour && timeNow.minute >= timeSince.minute) || timeNow.hour > timeSince.hour)
        let before: Bool = ((timeNow.hour == timeBefore.hour && timeNow.minute < timeBefore.minute) || timeNow.hour < timeBefore.hour)
        backgroundColor = since && before ? UIColor.white.withAlphaComponent(0.4) : .white
        
        self.timeSince = CGFloat(timeSince.hour * 3600 + timeSince.minute * 60)
        self.timeBefore = CGFloat(timeBefore.hour * 3600 + timeBefore.minute * 60)
        secAll = self.timeBefore - self.timeSince
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
            let minutes = calendar.component(.minute, from: date)
            let sec = calendar.component(.second, from: date)
            self.nowTime = hour * 3600 + minutes * 60 + sec
        }
    }
    
    override func prepareForReuse() {
        subject.text = ""
        time.text = ""
        place.text = ""
        backgroundColor = UIColor.white.withAlphaComponent(0.0)
        if timer != nil {
            timer.fire()
        }
    }
}
