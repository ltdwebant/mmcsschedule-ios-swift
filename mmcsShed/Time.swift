//
//  File.swift
//  mmcsShed
//
//  Created by Даниил Чемеркин on 07/09/2018.
//  Copyright © 2018 Danya. All rights reserved.
//

import Foundation

struct Time {
    let hour: Int
    let minute: Int
//    
//    func comparison(a: Time, b: Time) -> Bool {
//        if a.hour > b.hour {
//            return true
//        } else if a.hour == b.hour && a.minute > b.minute {
//            return true
//        } else 
//    }
    
    
    
    init(time: String) {
        let _time = time.split(separator: ":")
        hour = Int(_time[0])!
        minute = Int(_time[1])!
    }
    
    static var nowTime: Time {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return Time(time: "\(hour):\(minutes)")
    }
    
}
